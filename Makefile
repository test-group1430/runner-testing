# Read the version from VERSION file
VERSION := $(shell cat VERSION)

BUILD := $(shell cat BUILD)

# Default target
all: build

# Build target
build:
	@echo "Building version $(VERSION) (Build $(BUILD))"
	npm run build
	@echo $$(($$(cat BUILD) + 1)) > BUILD
	@echo "New build version $(BUILD)"
	@echo "Build number incremented"

# Target to update the version
set-version:
	@read -p "Enter new version number: " new_version; \
	echo $$new_version > VERSION; \
	echo "Version updated to $$new_version"

.PHONY: all build set-version
